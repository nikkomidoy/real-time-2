# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2017-12-03 18:22
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0008_auto_20171203_1142'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='questions',
            field=jsonfield.fields.JSONField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='campaign',
            name='template',
            field=models.CharField(blank=True, choices=[(26, 'Notification'), (27, 'Protection'), (30, 'Retention'), (28, 'Access & Correction'), (32, 'Openness'), (25, 'Purpose'), (31, 'Transfer'), (10, 'Personal Data'), (94, 'test sections'), (95, 'New section'), (96, 'New test section'), (9, 'Consent')], max_length=1, null=True),
        ),
    ]
