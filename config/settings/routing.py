from channels.staticfiles import StaticFilesConsumer
from channels.routing import route
from campaign import consumers


channel_routing = [
    route('websocket.connect', consumers.ws_connect, path=r"^/(?P<room_name>[a-zA-Z0-9_]+)/$"),
    route('websocket.receive', consumers.ws_receive, path=r"^/(?P<room_name>[a-zA-Z0-9_]+)/$"),
    route('websocket.disconnect', consumers.ws_disconnect, path=r"^/(?P<room_name>[a-zA-Z0-9_]+)/$"),

]
