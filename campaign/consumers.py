import json

from channels import Group
from channels.auth import channel_session_user, channel_session_user_from_http
from channels.sessions import channel_session
from django.core.urlresolvers import reverse_lazy, reverse

from .models import (
    GameRoom,
    GamePlayer,
)


@channel_session
def ws_connect(message, room_name):
    Group(room_name, channel_layer=message.channel_layer).add(message.reply_channel)
    message.reply_channel.send({"accept": True})
    message.channel_session['room'] = room_name


@channel_session
def ws_receive(message, room_name):
    data = json.loads(message.get('text',''))
    room_label = message.channel_session['room']
    if 'start' in data.keys():
        gameroom = GameRoom.objects.get(game_pin=data.get('game_pin'))
        Group(room_label).send({
            'text': json.dumps({
                'start': data.get('start'),
                'gameroom_id': gameroom.id
            })
        })
    elif 'choices' in data.keys():
        Group(room_label).send({
            'text': json.dumps({
                'choices': data.get('choices'),
                'question_index': data.get('question_index'),
                'campaign_id': data.get('campaign_id')
            })
        })
    elif 'answer' in data.keys():
        player = GamePlayer.objects.get(id=int(data.get('player_id')))
        player.update_score(int(data.get('answer')))
        player.save()

        q_idx = int(data.get('question_index'))
        q_idx += 1

        url = "{}?campaignid={}&qidx={}".format(
            reverse_lazy('campaign:questions'),
            int(data.get('campaign_id')),
            q_idx,
        )

        Group(room_label).send({
            'text': json.dumps({
                'answer': data.get('answer'),
                'next_question': url
            })
        })
    elif 'end' in data.keys():
        Group(room_label).send({
            'text': json.dumps({
                'end': data.get('end'),
            })
        })
    else:
        game_pin = data.get('game_pin','')
        try:
            gameroom = GameRoom.objects.get(game_pin=game_pin)
            Group(room_label).send({
                'text': json.dumps({
                    'nickname': data.get('nickname',''),
                    'game_pin': data.get('game_pin',''),
                    'campaign_id': gameroom.campaign.id,
                })
            })
        except GameRoom.DoesNotExist:
            Group(room_label).send({
                'text': json.dumps({
                    'error': "Game Pin error"
                })
            })


@channel_session_user
def ws_disconnect(message, room_name):
    room_label = message.channel_session['room']
    Group(room_label, channel_layer=message.channel_layer).discard(message.reply_channel)
