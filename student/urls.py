from django.conf.urls import url

from . import views


urlpatterns = [
    url(
        regex=r'^$',
        view=views.StudentGameView.as_view(),
        name='student_game'
    ),
    url(
        regex=r'quiz/$',
        view=views.TakeQuizView.as_view(),
        name='take_quiz'
    ),
    url(
        regex=r'end/$',
        view=views.EndGameView.as_view(),
        name="end_game"
    ),
    url(
        regex=r'waiting/(?P<pk>\d+)/$',
        view=views.StudentWaitingView.as_view(),
        name='student_waiting'
    ),
]
