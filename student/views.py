import json

from django.views.generic import (
    TemplateView,
    UpdateView,
    ListView,
    DetailView,
)
from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect
from websocket import create_connection

from campaign.models import (
    Campaign,
    GameRoom,
    GamePlayer,
)


class StudentGameView(TemplateView):
    template_name = "student/student_game.html"

    def post(self, request, *args, **kwargs):
        game_pin = request.POST.get('game_pin')
        nickname = request.POST.get('nickname')
        player = None
        try:
            gameroom = GameRoom.objects.get(game_pin=game_pin)
            url = "{}{}".format(settings.WS_URL, "/stream/")
            ws = create_connection(url)
            ws.send(json.dumps({
                'nickname': nickname,
                'game_pin': game_pin
            }))
            player = GamePlayer.objects.create(name=nickname, campaign=gameroom.campaign)
            player.label = "{}-{}".format(player.name,player.id)
            player.save()
            url = "{}?nickname={}&id={}".format(
                reverse_lazy('student:student_waiting', kwargs={'pk': gameroom.pk}),
                nickname,
                player.id
            )
            return HttpResponseRedirect(url)
        except GameRoom.DoesNotExist:
            print("GameRoom does not exist")
            messages.error(self.request, "Game Room does not exist")
            return HttpResponseRedirect(reverse_lazy('student:student_game'))


class StudentWaitingView(DetailView):
    template_name = "student/student_waiting.html"
    model = GameRoom

    def get_context_data(self, **kwargs):
        context = super(StudentWaitingView, self).get_context_data(**kwargs)
        gameroom = GameRoom.objects.get(pk=self.kwargs.get('pk'))
        context['room'] = gameroom.campaign.label
        context['nickname'] = self.request.GET.get('nickname')
        context['player_id'] = self.request.GET.get('id')
        return context


class TakeQuizView(TemplateView):
    template_name = "student/take_quiz.html"

    def get_context_data(self, **kwargs):
        context = super(TakeQuizView, self).get_context_data(**kwargs)
        campaign_id = self.request.GET.get('campaignid', '')
        if campaign_id:
            campaign = Campaign.objects.get(id=campaign_id)
            q_idx = int(self.request.GET.get('qidx'))
            data = json.loads(campaign.questions)
            context['choices'] = [choice for choice in data[q_idx].get('choices')]
            context['qidx'] = q_idx
            context['campaignid'] = campaign_id

        context['nickname'] = self.request.GET.get('nickname')
        context['player_id'] = self.request.GET.get('id')
        return context


class EndGameView(TemplateView):
    template_name = "student/end_game.html"

    def get_context_data(self, **kwargs):
        context = super(EndGameView, self).get_context_data(**kwargs)
        player_id = int(self.request.GET.get('id'))
        player = GamePlayer.objects.get(id=player_id)
        context['player'] = player
        return context
