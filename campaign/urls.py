from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    url(
        regex=r'^$',
        view=views.CampaignListView.as_view(),
        name='list'
    ),
    url(
        regex=r'create/$',
        view=views.CampaignCreateView.as_view(),
        name="create_campaign"
    ),
    url(
        regex=r'start/(?P<pk>\d+)/$',
        view=views.StartGameView.as_view(),
        name="start"
    ),
    url(
        regex=r'config/(?P<pk>\d+)/$',
        view=views.CampaignConfigView.as_view(),
        name="config"
    ),
    url(
        regex=r'questions/$',
        view=views.QuestionGameView.as_view(),
        name="questions"
    ),
    url(
        regex=r'(?P<pk>\d+)/preview/$',
        view=views.CampaignPreviewView.as_view(),
        name="preview_campaign"
    ),
    url(
        regex=r'(?P<pk>\d+)/$',
        view=views.CampaignUpdateView.as_view(),
        name="update_campaign"
    ),
    url(
        regex=r'last/$',
        view=views.EndGameView.as_view(),
        name="end_game"
    ),
]
