import json
import random
import string

from channels import Group
from django.conf import settings
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template.defaultfilters import slugify
from django.utils.html import strip_tags
from django.views.generic import (
    TemplateView,
    UpdateView,
    ListView,
    DetailView,
    View,
)
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from websocket import create_connection

from .api import format_questions_by_json, group_question_by_sections
from .models import (
    Campaign,
    GameRoom,
    GamePlayer,
)


class CampaignCreateView(LoginRequiredMixin, CreateView):
    """
    This is view for creating Campaign
    """
    template_name = "campaign/create_campaign.html"
    model = Campaign
    fields = ['name', 'description', 'template']

    def get_success_url(self):
        if not self.object.label:
            random_str = ''.join(
                random.choices(string.ascii_uppercase + string.digits, k=10)
            )
            self.object.label = slugify("{}-{}".format(self.object.name, random_str))

        self.object.questions = format_questions_by_json(self.object.template)
        self.object.save()
        return reverse(
            'campaign:preview_campaign',
            kwargs={'pk': self.object.pk}
        )


class CampaignUpdateView(LoginRequiredMixin, UpdateView):
    """
    This is view for updating Campaign
    """
    template_name = "campaign/update_campaign.html"
    fields = ['name', 'description', 'template']
    model = Campaign

    def get_success_url(self):
        if not self.object.label:
            random_str = ''.join(
                random.choices(string.ascii_uppercase + string.digits, k=10)
            )
            self.object.label = slugify("{}-{}".format(self.object.name, random_str))

        self.object.questions = format_questions_by_json(self.object.template)
        self.object.save()
        return reverse(
            'campaign:preview_campaign',
            kwargs={'pk': self.object.pk}
        )

    def get_object(self):
        return Campaign.objects.get(id=self.kwargs.get('pk'))


class CampaignListView(LoginRequiredMixin, ListView):
    model = Campaign
    slug_field = 'pk'
    slug_url_kwarg = 'pk'


class StartGameView(LoginRequiredMixin, DetailView):
    """
    This is view for starting Campaign
    """
    template_name = "campaign/start_game.html"
    model = Campaign
    slug_field = 'pk'
    slug_url_kwarg = 'pk'

    def get_context_data(self, **kwargs):
        context = super(StartGameView, self).get_context_data(**kwargs)
        campaign = Campaign.objects.get(pk=self.kwargs.get('pk'))
        game_pin = ''.join(random.choices(string.ascii_uppercase + string.digits, k=7))
        GameRoom.objects.filter(campaign=campaign).update(is_enable=False)
        GameRoom.objects.create(
            game_pin=game_pin,
            campaign=campaign,
        )
        context['game_pin'] = game_pin

        return context

    def post(self, request, *args, **kwargs):
        campaign = Campaign.objects.get(pk=self.kwargs.get('pk'))

        url = "{}?campaignid={}&qidx={}".format(
            reverse_lazy('campaign:questions'),
            campaign.pk,
            0,
        )
        return HttpResponseRedirect(url)


class QuestionGameView(LoginRequiredMixin, TemplateView):
    template_name = "campaign/game_questions.html"

    def get_context_data(self, **kwargs):
        try:
            context = super(QuestionGameView, self).get_context_data(**kwargs)
            campaign_id = self.request.GET.get('campaignid')
            q_idx = int(self.request.GET.get('qidx'))
            campaign = Campaign.objects.get(pk=campaign_id)
            data = json.loads(campaign.questions)
            if "<img src=" in data[q_idx]['name']:
                data[q_idx]['name'] = data[q_idx]['name'].replace("\'", '"')

            context['question'] = data[q_idx]
            context['question_index'] = q_idx
            context['choices'] = data[q_idx].get('choices')
            context['campaign_id'] = campaign.id
            return context
        except Exception as e:
            url = "{}{}".format(settings.WS_URL, "/stream/")
            ws = create_connection(url)
            ws.send(json.dumps({
                'end': "end",
            }))


class EndGameView(LoginRequiredMixin, TemplateView):
    template_name = "campaign/end_game.html"

    def get_context_data(self, **kwargs):
        context = super(EndGameView, self).get_context_data(**kwargs)
        return context


class CampaignPreviewView(LoginRequiredMixin, TemplateView):
    template_name = "campaign/preview.html"

    def get_context_data(self, **kwargs):
        context = super(CampaignPreviewView, self).get_context_data(**kwargs)

        campaign = Campaign.objects.get(pk=self.kwargs.get('pk'))
        context['sections'] = group_question_by_sections(campaign.template)

        return context


class CampaignConfigView(LoginRequiredMixin, TemplateView):
    template_name = "campaign/config.html"

    def get_context_data(self, **kwargs):
        context = super(CampaignConfigView, self).get_context_data(**kwargs)
        return context
