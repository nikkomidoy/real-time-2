from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from jsonfield import JSONField

from .api import get_templates
from django.utils.functional import lazy


optional = {
    'blank': True,
    'null': True
}


class Campaign(models.Model):

    TEMPLATE_CHOICES =  get_templates()

    name = models.CharField(max_length=100)
    label = models.SlugField(unique=True, **optional)
    description = models.TextField()
    template = models.IntegerField(choices=TEMPLATE_CHOICES, **optional)
    questions = JSONField(**optional)

    def __str__(self):
        return self.name


class GameRoom(models.Model):
    game_pin = models.CharField(max_length=50, **optional)
    campaign = models.ForeignKey(Campaign, **optional)
    is_enable = models.BooleanField(default=True)

    def __str__(self):
        return self.campaign.name


class GamePlayer(models.Model):
    name = models.CharField(max_length=50, **optional)
    campaign = models.ForeignKey(Campaign, **optional)
    score = models.IntegerField(default=0, **optional)
    label = models.CharField(max_length=50, **optional)

    def update_score(self, answer):
        self.score += answer

    def __str__(self):
        return self.name
