import json
import requests

from django.conf import settings
from django.utils.html import strip_tags


def get_templates():
    data = []
    for idx in range(1,11):
        response = requests.get(
            "{}/ca-templates/api/temp/{}/".format(settings.API_URL,idx),
            auth=settings.API_AUTH
        )
        if response.status_code == 200:
            resp = response.json()
            data.append(
                (resp.get('id'), resp.get('name'))
            )
        else:
            break
    return data


def get_sections_by_template(template_id):
    """
    Get all sections in a template
    """
    response = requests.get(
        "{}/ca-templates/api/temp/{}/sects/".format(
            settings.API_URL, template_id),
        auth=settings.API_AUTH
    )
    return tuple([
            (resp.get('id'), resp.get('name'))
            for resp in response.json()
        ])


def get_questions_by_section_id(section_id):
    response = requests.get(
        "{}/ca-templates/api/sect/{}/quests/".format(settings.API_URL, section_id),
        auth=settings.API_AUTH
    )
    return response.json()


def get_answers_by_question_id(question_id):
    response = requests.get(
        "{}/ca-templates/api/quest/{}/ans/".format(settings.API_URL, question_id),
        auth=settings.API_AUTH
    )
    return [
        { resp.get('label'): resp.get('value') } for resp in response.json()
    ]


def format_questions_by_json(template_id):
    sections = get_sections_by_template(template_id)
    data = []
    for section in sections:
        for resp in get_questions_by_section_id(section[0]):
            name = resp.get('q_name')
            if name:
                data.append(
                    {
                        'name': strip_tags(name),
                        'description': resp.get('description'),
                        'choices': get_answers_by_question_id(resp.get('id'))
                    }
                )
    return json.dumps(data)


def group_question_by_sections(template_id):
    sections = get_sections_by_template(template_id)
    data = []
    for section in sections:
        temp = []
        for resp in get_questions_by_section_id(section[0]):
            temp.append(
                {
                    'id': resp.get('id'),
                    'name': strip_tags(resp.get('q_name')),
                }
            )
        data.append({
            section[1]: temp
        })

    return data


def get_question_by_index(data, index):
    items = json.loads(data)
    return items[index]
